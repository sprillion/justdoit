﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fan : MonoBehaviour
{
    [SerializeField] private Animator animator;
    [SerializeField] private Micro micro;
    private float threshold = 0.5f;

    private void Awake()
    {
        State = FanState.Stop;
    }

    private void Update()
    {
        if (GameManager.playing && micro.loudness > threshold)
        {
            State = FanState.Rotation;
        }
        else
        {
            State = FanState.Stop;
        }
    }

    public FanState State
    {
        get { return (FanState)animator.GetInteger("State"); }
        set { animator.SetInteger("State", (int)value); }
    }
}
public enum FanState
{
    Rotation,
    Stop
}

