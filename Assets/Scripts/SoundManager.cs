﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class SoundManager : MonoBehaviour
{
    private static GameObject soundGameObject;
    private static AudioSource audioSource;
    public enum Sound
    {
        Boom,
        Click,
    }
    public static void PlaySound(Sound sound)
    {
        if (!GameManager.playSound) return;
        if (soundGameObject == null)
        {
            soundGameObject = new GameObject("Sound");
            audioSource = soundGameObject.AddComponent<AudioSource>();
        }

        audioSource.PlayOneShot(GetAudioClip(sound));
    }

    private static AudioClip GetAudioClip(Sound sound)
    {
        return (from e in GameAssets.i.SoundAudioClipArray where e.sound == sound select e.audioClip).FirstOrDefault();
    }
}
