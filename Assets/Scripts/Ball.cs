﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ball : MonoBehaviour
{

    [SerializeField] private Micro micro;
    private float threshold = 1f;
    [SerializeField] public Rigidbody2D rb;
    [SerializeField] public Animator animator;
    [SerializeField] private GameManager game;
    [SerializeField] private int force = 15;


    private void FixedUpdate()
    {
        if (micro.loudness > threshold)
        {
            rb.AddForce(-(transform.up * micro.loudness * force), ForceMode2D.Force);
        }
        else
        {
            rb.AddForce(new Vector2(), ForceMode2D.Force);
        }
    }

    private void OnTriggerEnter2D(Collider2D collider)
    {
        if (collider.gameObject.tag != "Enemy") return;
        animator.Play("Boom");
        SoundManager.PlaySound(SoundManager.Sound.Boom);
        game.EndGame();
    }

}
