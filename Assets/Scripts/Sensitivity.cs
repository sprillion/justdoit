﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Sensitivity : MonoBehaviour
{
    [SerializeField] private Slider slider;

    private void Start()
    {
        slider.minValue = 10000;
        slider.maxValue = 200000;
        slider.value = Micro.sensitivity;
    }

    public void SetSens()
    {
        Micro.sensitivity = slider.value;
        PlayerPrefs.SetFloat("Sens",Micro.sensitivity);
    }
}
