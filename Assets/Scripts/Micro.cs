﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Android;
using UnityEngine.SceneManagement;

public class Micro : MonoBehaviour
{
    private string div;
    [SerializeField] private AudioSource aud;
    public static float sensitivity = 100000;
    public float loudness = 0;
    private int timeMicro = 240;

    void Start()
    {
        sensitivity = PlayerPrefs.HasKey("Sens") ? PlayerPrefs.GetFloat("Sens") : 100000;

#if UNITY_ANDROID && !UNITY_EDITOR
        StartCoroutine(StartMic());
#endif
#if UNITY_EDITOR
    FoundDevices();
#endif
    }

    private IEnumerator StartMic()
    {
        if (!Permission.HasUserAuthorizedPermission(Permission.Microphone))
        {
            Permission.RequestUserPermission(Permission.Microphone);
            yield return new WaitForSeconds(1f);
            SceneManager.LoadScene(0);
        }
        FoundDevices();

    }

    private void FoundDevices()
    {
        foreach (string device in Microphone.devices)
        {
            Debug.Log("Name: " + device);
            div = device;
        }
        StartCoroutine(Mic());
    }

    private IEnumerator Mic()
    {
        while (true)
        {
            aud.clip = Microphone.Start(div, true, timeMicro, 44100);
            while (!(Microphone.GetPosition(div) > 0)) { }
            aud.Play();
            yield return new WaitForSecondsRealtime(timeMicro);
        }
    }

    private void Update()
    {
        loudness = GetAveragedVolume() * sensitivity;
    }

    float GetAveragedVolume()
    {
        float[] data = new float[256];
        aud.GetOutputData(data, 0);
        float a = data.Sum(s => Mathf.Abs(s));
        return a / 256;
    }
}
