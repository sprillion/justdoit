﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mime;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    [SerializeField] private GameObject enemy;
    [SerializeField] private Ball ball;

    [SerializeField] private GameObject playButton;
    [SerializeField] private GameObject endGamePanel;

    [SerializeField] private Text currentScoreText;
    [SerializeField] private Text allScoreText;
    [SerializeField] private Text recordText;


    private List<GameObject> enemiesList = new List<GameObject>();

    private float interval = 2f;
    private float startXEnemy = 4.5f;

    public static bool playing = false;


    private int allScore = 0;
    public int currentScore = 0;



    private void Start()
    {
        allScore = PlayerPrefs.HasKey("Score") ? PlayerPrefs.GetInt("Score") : 0;
        recordText.text = PlayerPrefs.HasKey("Best") ? "Рекорд: " + PlayerPrefs.GetInt("Best") : "";

    }

    private void Update()
    {
        CloseApp();
    }

    private Coroutine reloadRoutine;
    public void StartGame()
    {
        playing = true;
        reloadRoutine = StartCoroutine(EnemiesPool());
        ball.rb.simulated = true;
        currentScore = 0;
        recordText.text = "";

        SoundManager.PlaySound(SoundManager.Sound.Click);

    }

    public void EndGame()
    {
        endGamePanel.SetActive(true);
        playing = false;
        StopCoroutine(reloadRoutine);
        ball.rb.simulated = false;
        foreach (var e in enemiesList)
        {
            e.SetActive(false);
        }

        currentScoreText.text = currentScore + "";
        if (!PlayerPrefs.HasKey("Best") || PlayerPrefs.GetInt("Best") < currentScore)
                PlayerPrefs.SetInt("Best", currentScore);

        allScore += currentScore;
        PlayerPrefs.SetInt("Score", allScore);
    }

    public void RefreshGame()
    {
        SoundManager.PlaySound(SoundManager.Sound.Click);
        endGamePanel.SetActive(false);
        //Invoke("StartPosition", 0.4f);
        StartPosition();

        playButton.SetActive(true);
        recordText.text = "Рекорд: " + PlayerPrefs.GetInt("Best");
    }

private void StartPosition()
    {
        ball.transform.position = new Vector3(0, -2.9f);
        ball.animator.Play("Calm");
    }

    private IEnumerator EnemiesPool()
    {
        while (playing)
        {
            yield return new WaitForSeconds(interval);
            var r = RandomSide();
            var find = false;
            foreach (var e in enemiesList.Where(e => !e.activeSelf))
            {
                e.transform.position = new Vector3(startXEnemy * r, RandomHeight());
                e.transform.localScale = new Vector3(r, 1);
                e.GetComponent<Enemy>().side = -r;
                e.SetActive(true);
                find = true;
                break;
            }

            if (find) continue;
            enemiesList.Add(Instantiate(enemy, new Vector3(startXEnemy * r, RandomHeight()), new Quaternion()));
            enemiesList[enemiesList.Count - 1].transform.localScale = new Vector3(r, 1);
            enemiesList[enemiesList.Count - 1].GetComponent<Enemy>().side = -r;
        }

    }

    private int RandomSide()
    {
        if (Random.Range(0, 2) == 0)
            return -1;
        return 1;

    }

    private float RandomHeight()
    {
        return Random.Range(-3.2f, 2.8f);
    }

    private void CloseApp()
    {
        if (!Input.GetKeyDown(KeyCode.Escape)) return;
        SoundManager.PlaySound(SoundManager.Sound.Click);

        if (endGamePanel.activeSelf)
            RefreshGame();
        else
            Application.Quit();
    }

    public static bool playSound = true;

    public void SwitchModeSound()
    {
        playSound = !playSound;
    }
}
