﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    public int side;

    [SerializeField] private float speed = 5f;
    [SerializeField] private GameManager game;

    private void Awake()
    {
        game = FindObjectOfType<GameManager>();
    }
    private void Update()
    {
        
        transform.position =
            Vector3.MoveTowards(transform.position, new Vector3(4.5f * side, transform.position.y), speed * Time.deltaTime);
        if (transform.position == new Vector3(4.5f * side, transform.position.y))
        {
            game.currentScore++;
            gameObject.SetActive(false);
        }
    }
}
